package ru.equestriadev.myweather;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SearchView;


public class MainActivity extends ActionBarActivity implements SearchView.OnQueryTextListener {

    de.timroes.android.listview.EnhancedListView list;
    GetWeather getWeather;
    SwipeRefreshLayout layout;
    SearchView searchView;
    WeatherSettings settings;
    int position;
    Bundle savedInstanceState;
    String requests[]={
            "http://api.openweathermap.org/data/2.5/weather?q=London",
            "http://api.openweathermap.org/data/2.5/weather?q=Minsk",
            "http://api.openweathermap.org/data/2.5/weather?q=Paris",
            "http://api.openweathermap.org/data/2.5/weather?q=New_York",
            "http://api.openweathermap.org/data/2.5/weather?q=Mahilyow",
            "http://api.openweathermap.org/data/2.5/weather?q=Brest,by",
            "http://api.openweathermap.org/data/2.5/weather?q=vitebsk,by",
            "http://api.openweathermap.org/data/2.5/weather?q=gomel,by",
            "http://api.openweathermap.org/data/2.5/weather?q=vitebsk,by",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        list=(de.timroes.android.listview.EnhancedListView)findViewById(R.id.listview);
        layout=(SwipeRefreshLayout)findViewById(R.id.refresh);
        settings= new WeatherSettings(getApplicationContext());
        requests=settings.generateRequests();
        this.savedInstanceState=savedInstanceState;
        load();
        layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                load();
            }
        });


    }
public void load()
{
    if(getWeather!=null)
        getWeather.cancel(true);
    getWeather = new GetWeather();
    getWeather.context=getApplicationContext();
    getWeather.list=list;
    getWeather.refresh=layout;
    if(savedInstanceState!=null)
    {
        //getWeather.request=savedInstanceState.getString("Search");
        getWeather.listposition=savedInstanceState.getInt("ListPositon");
        savedInstanceState=null;
    }
    getWeather.execute(requests);
}
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
       getMenuInflater().inflate(R.menu.menu_main, menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.menu_item_search).getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setSubmitButtonEnabled(false);
        searchView.setOnQueryTextListener(this);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if(id==R.id.addnew)
        {
            showDial();
        }

        return super.onOptionsItemSelected(item);
    }
    public void showDial()
    {
        final AlertDialog.Builder adb = new AlertDialog.Builder(this);
        adb.setTitle("Add new town");
        adb.setCancelable(true);
        // создаем view из dialog.xml
        RelativeLayout view = (RelativeLayout) getLayoutInflater()
                .inflate(R.layout.alert_add, null);
        // устанавливаем ее, как содержимое тела диалога
        final EditText editor = (EditText)view.findViewById(R.id.editText);
        adb.setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        settings.addTown(editor.getText().toString());
                        requests=settings.generateRequests();
                        load();

                    }
                }
        );
        adb.setNegativeButton("Don't add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //NOTHING!!!
            }
        });
        adb.setView(view);
        adb.create();
        adb.show();
    }
    @Override
    public boolean onQueryTextChange(String newText)
    {
        ListAdapter adapter = (ListAdapter) list.getAdapter();
       if(adapter!=null) {
           Log.d("Debug", "Try to filter it: " + newText);

           adapter.getFilter().filter(newText);
       }
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        // TODO Auto-generated method stub
        return false;
    }

    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d("Debug", "onSaveInstanceState");
        outState.putInt("ListPositon", list.getFirstVisiblePosition());
        outState.putString("Search", searchView.getQuery().toString());
    }
}
