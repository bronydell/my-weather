package ru.equestriadev.myweather;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.widget.SearchView;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import de.timroes.android.listview.EnhancedListView;

/**
 * Created by Rostislaw on 6/3/2015.
 */
public class GetWeather extends  AsyncTask<String, Void, Void> {

    JSONObject json;
    Context context;
    WeatherManager weatherManager;
    WeatherSettings weatherSettings;
    de.timroes.android.listview.EnhancedListView list;
    SwipeRefreshLayout refresh;
    SearchView searchView;
    int listposition=0;
    String request;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        weatherManager = new WeatherManager(context);
        weatherSettings = new WeatherSettings(context);
        if(refresh!=null)
            refresh.setRefreshing(true);
    }

    @Override
    protected Void doInBackground(String... urls) {
        for(int i=0;i<urls.length;i++) {

            json = loadJSON(urls[i]);
            if(json!=null)
            parseWeather();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        final ArrayList<Weather> weathers;
        weathers=weatherManager.ReadWeather();
        final ListAdapter adapter = new ListAdapter(context, weathers);
        list.setAdapter(adapter);
        list.setSelection(listposition);
        if(request!=null)
        searchView.setQuery(request, false);
        if(refresh!=null)
            refresh.setRefreshing(false);
        list.setDismissCallback(new EnhancedListView.OnDismissCallback() {
            @Override
            public EnhancedListView.Undoable onDismiss(EnhancedListView enhancedListView, int i) {
                weatherManager.removeFromDB(adapter.getItem(i).getTown());
                weatherSettings.removeTown(adapter.getItem(i).getTown());
                weathers.remove(adapter.getItem(i));
                adapter.notifyDataSetChanged();
                return null;
            }
        });
        list.enableSwipeToDismiss();

        Log.d("Debug", "--- END CODE LOGIC ---");
        this.cancel(true);
    }

    public void parseWeather()
    {
        Weather weather = new Weather();

        try {
            if(json.has("cod")) {
                weather.setTown(json.getString("name"));
                weather.setCloud(json.getJSONArray("weather").getJSONObject(0).getString("description"));
                weather.setStatus(0);
                weather.setWind(json.getJSONObject("wind").getString("deg") + (char) 0x00B0);
                DecimalFormat dof = new DecimalFormat("#.#");
                weather.setTempirature(dof.format(json.getJSONObject("main").getDouble("temp") - 273.15) + "C" + (char) 0x00B0);
                DateFormat df = new SimpleDateFormat("HH:mm");
                weather.setSunrise(df.format(new java.util.Date(json.getJSONObject("sys").getLong("sunrise") * 1000)).toString());
            }
            else {
                Log.d("Debug", "No-no-no! ERROOOR!");
                weather = null;
            }
        }
        catch ( JSONException e) {
            e.printStackTrace();
        }
        if(weather!=null)
            weatherManager.WriteWeather(weather);
    }
    public JSONObject loadJSON(String url)
    {
        JSONObject json = null;
        String str = "";
        HttpResponse response;
        HttpClient myClient = new DefaultHttpClient();
        HttpPost myConnection = new HttpPost(url);
        try {
            response = myClient.execute(myConnection);
            str = EntityUtils.toString(response.getEntity(), "UTF-8");

        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            json = new JSONObject(str);


        } catch ( JSONException e) {
            e.printStackTrace();
        }
    return json;
    }

}
