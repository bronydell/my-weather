package ru.equestriadev.myweather;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by Rostislaw on 6/3/2015.
 */
public class WeatherManager {


    DBHelper dbManager;
    SQLiteDatabase writablebase;
    Context context;

    public WeatherManager(Context context)
    {
        this.context = context;
        dbManager = new DBHelper(this.context);
        writablebase = dbManager.getWritableDatabase();
    }

    public void WriteWeather(Weather weather)
    {
        Log.d("Debug", "-- WRITE TO DB! --");
        ContentValues cv = new ContentValues();
        cv.put("town", weather.getTown());
        cv.put("cloud", weather.getCloud());
        cv.put("tempirature", weather.getTempirature());
        cv.put("wind", weather.getWind());
        cv.put("sunrise", weather.getSunrise());
        cv.put("status", weather.getStatus());
        Log.d("Debug", cv+"");
        if(getWeatherPosition(weather.getTown())==-1&&weather.getTown()!=null) writablebase.insert("weathertable", null, cv);
        else {
            writablebase.update("weathertable", cv, "town" + "= ?", new String[]{weather.getTown()});
        }
    }

    public ArrayList<Weather> ReadWeather(){
        Log.d("Debug", "-- READ FROM DB! --");
        Cursor c = writablebase.query("weathertable", null, null, null, null, null, null);
        ArrayList<Weather> weathers = new ArrayList<>();
        while (c.moveToNext()) {
            Weather weather = new Weather();
            weather.setTown(c.getString(c.getColumnIndex("town")));
            //Log.d("Debug", "Read from DB" + c.getString(c.getColumnIndex("town")));
            weather.setCloud(c.getString(c.getColumnIndex("cloud")));
            weather.setTempirature(c.getString(c.getColumnIndex("tempirature")));
            weather.setWind(c.getString(c.getColumnIndex("wind")));
            weather.setSunrise(c.getString(c.getColumnIndex("sunrise")));
            weather.setStatus(c.getInt(c.getColumnIndex("status")));
            weathers.add(weather);
        }


        Log.d("Debug", "Result: weathers size = " + weathers.size());
        return weathers;
    }
    public void removeFromDB(String townname)
    {
        Log.d("Debug", "-- REMOVE FROM DB! --");
        writablebase.delete("weathertable", "town"+"= ?", new String[]{townname});
    }
    public int getWeatherPosition(String townname)
    {
        Log.d("Debug", "-- GET POSITION FROM DB! --");
        Cursor c = writablebase.query("weathertable", null, null, null, null, null, null);

                int index = c.getColumnIndex("town");
                while (c.moveToNext()){
                  if(c.getString(index).equals(townname)) {
                      Log.d("Debug", "Result of searching DB pos = " + c.getPosition());
                      return c.getPosition();
                  }
                }

        return -1;
            }

    }

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, "myDB", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("Debug", "--- onCreate database ---");
            db.execSQL("create table weathertable ("
                    + "town text primary key,"
                    + "cloud text,"
                    + "tempirature text,"
                    + "wind text,"
                    + "sunrise text,"
                    + "status integer" + ");");

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.d("Debug", "--- onUpgrade database ---");
            if (oldVersion >= newVersion)
                return;
            db.execSQL("DROP TABLE IF EXISTS weathertable");
                    onCreate(db);
        }
    }

