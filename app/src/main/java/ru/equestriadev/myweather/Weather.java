package ru.equestriadev.myweather;

/**
 * Created by Rostislaw on 6/3/2015.
 */
public class Weather {
    private String town;
    private String tempirature;
    private String sunrise;
    private String wind;
    private String cloud;
    private int status;

    public void setCloud(String cloud)
    {
        this.cloud=cloud;
    }

    public void setTown(String town)
    {
        this.town=town;
    }

    public void setTempirature(String tempirature)
    {
        this.tempirature=tempirature;
    }

    public void setSunrise(String sunrise)
    {
        this.sunrise=sunrise;
    }

    public void setWind(String wind)
    {
        this.wind=wind;
    }

    public void setStatus(int status)
    {
        this.status=status;
    }

    public String getTown()
    {
        return town;
    }

    public String getTempirature()
    {
        return tempirature;
    }

    public String getSunrise()
    {
        return sunrise;
    }

    public String getWind()
    {
        return wind;
    }

    public String getCloud(){ return cloud; }

    public int getStatus()
    {
        return status;
    }


}
