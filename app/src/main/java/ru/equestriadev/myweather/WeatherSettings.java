package ru.equestriadev.myweather;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;

/**
 * Created by Rostislaw on 6/7/2015.
 */
public class WeatherSettings {

    String SAVED_TOWNS = "SaveTown";
    Gson gson;
    ArrayList<String> townlist;
    private SharedPreferences preferences;

    public WeatherSettings(Context context) {
        preferences = context.getSharedPreferences("WeatherSettings", context.MODE_PRIVATE);
        townlist = new ArrayList<>();
        gson = new Gson();
        if (!preferences.getString(SAVED_TOWNS, "Error 404").equals("Error 404"))
            townlist = gson.fromJson(preferences.getString(SAVED_TOWNS, "Error 404"), new TypeToken<ArrayList<String>>() {
            }.getType());
        else {
            townlist.add("Minsk");
            saveTowns();
        }

    }

    private void saveTowns()
    {
     preferences.edit().putString(SAVED_TOWNS, gson.toJson(townlist)).commit();
    }

    public void addTown(String townName)
    {
        boolean cont = true;
        for(String item : townlist) {
            if(townName.equalsIgnoreCase(item)) {
                cont=false;
            }
        }
        if(cont) {
            townlist.add(townName.toLowerCase().replace(" ",""));
            saveTowns();
        }
        else
        {
            Log.d("Debug", "This town is exist!");
        }
    }

    public void removeTown(String townName)
    {
        for(int i=0;i<townlist.size();i++)
        {
            if(townlist.get(i).toLowerCase().equals(townName.toLowerCase())) {
                townlist.remove(i);
                Log.d("Debug", "Removed: "+i);
            }
        }
        saveTowns();

    }

    public ArrayList<String> getTownlist()
    {
        return townlist;
    }

    public String[] generateRequests()
    {
        String [] requests = new String[townlist.size()];
        for(int i=0;i<townlist.size();i++)
            requests[i]="http://api.openweathermap.org/data/2.5/weather?q="+townlist.get(i)+"&9c038f15284278d676d5261f97e7f5dc";
        return requests;
    }
}
