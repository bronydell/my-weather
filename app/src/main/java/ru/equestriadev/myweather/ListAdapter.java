package ru.equestriadev.myweather;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rostislaw on 6/5/2015.
 */
public class ListAdapter extends BaseAdapter implements Filterable {
    private final Context context;
    private ArrayList<Weather> weathers;
    private ArrayList<Weather> filterWeathers;


    private TextView town;
    private TextView tempirature;
    private TextView cloud;
    private TextView sunrise;
    private TextView wind;

    private ItemFilter mFilter = new ItemFilter();

    public ListAdapter(Context context, ArrayList<Weather> weathers) {
        this.context = context;
        this.weathers = weathers;
        this.filterWeathers=weathers;
    }

    public int getCount() {
        // TODO Auto-generated method stub
        return filterWeathers.size();
    }

    public Weather getItem(int pos) {
        // TODO Auto-generated method stub
        return filterWeathers.get(pos);
    }

    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_item, parent, false);
        town = (TextView) rowView.findViewById(R.id.townText);
        tempirature = (TextView) rowView.findViewById(R.id.gradText);
        cloud = (TextView) rowView.findViewById(R.id.statusText);
        sunrise = (TextView) rowView.findViewById(R.id.sunriseText);
        wind = (TextView) rowView.findViewById(R.id.windText);
        updateUI(filterWeathers.get(position));
        return rowView;
    }

    public Filter getFilter() {
        return mFilter;
    }

    public void updateUI(Weather weather) {
        town.setText(weather.getTown());
        tempirature.setText(weather.getTempirature());
        cloud.setText(weather.getCloud());
        sunrise.setText(weather.getSunrise());
        wind.setText(weather.getWind());
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            String filterString = constraint.toString().toLowerCase();

            FilterResults results = new FilterResults();

            final List<Weather> list = weathers;

            int count = list.size();
            final ArrayList<Weather> nlist = new ArrayList<Weather>(count);

            String filterableString;

            for (int i = 0; i < count; i++) {
                filterableString = list.get(i).getTown();
                if (filterableString.toLowerCase().contains(filterString)) {
                    nlist.add(list.get(i));
                }
            }

            results.values = nlist;
            results.count = nlist.size();

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filterWeathers = (ArrayList<Weather>) results.values;
            notifyDataSetChanged();
        }
    }
}
